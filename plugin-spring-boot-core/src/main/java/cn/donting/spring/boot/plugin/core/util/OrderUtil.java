package cn.donting.spring.boot.plugin.core.util;

import cn.donting.spring.boot.plugin.core.plugin.Order;

import java.util.*;

/**
 * @author donting
 * 2020-09-28 16:16
 */
public class OrderUtil {

    public static <T extends Order> List<T> orderByOrder(List<T> list,boolean reverse) {
        if(reverse){
            list.sort((o1, o2) -> o1.order() < o2.order() ? 1 : -1);
        }else{
            list.sort((o1, o2) -> o1.order() > o2.order() ? 1 : -1);
        }
        return list;
    }

    public static <T extends Order> List<T> orderByOrder(Map<String, T> map,boolean reverse) {
        List<T> list = new ArrayList<>(map.values());
        return orderByOrder(list,reverse);
    }

    public static void main(String[] args) {


    }


}
