package cn.donting.spring.boot.plugin.core.loader.classes;

import java.net.URL;

/**
 * @author donting
 * 2020-09-25 09:46
 */
public class PluginManagerClassLoader extends PluginClassLoader{

    public PluginManagerClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent,"manager-classloader");
    }

    @Override
    public void addURL(URL url) {
        super.addURL(url);
    }
}
