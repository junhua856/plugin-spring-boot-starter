package cn.donting.spring.boot.plugin.core.logging;

import java.util.logging.Logger;

/**
 * @author donting
 * 2020-09-12 20:34
 */
public class Log {

    private final Logger logger;

    public Log(Class c) {
        logger=Logger.getLogger(c.getCanonicalName());
    }

    public void info(String msg){
        logger.info(msg);
    }
    public void error(String msg){
        logger.severe(msg);
    }

    public void warning(String msg){
        logger.warning(msg);
    }



}
