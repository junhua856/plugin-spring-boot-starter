package cn.donting.spring.boot.plugin.core.plugin.manger;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.PluginWebSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.DoService;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInstallDb;
import cn.donting.spring.boot.plugin.core.servlet.IPluginManagerDispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * @author donting
 * 2020-09-25 11:35
 */
public class PluginBootWebManager extends PluginBootManager {

    public PluginBootWebManager(PluginInstallDb pluginInstallDb,String id,PluginSpringApplicationContext applicationContext) {
        super(pluginInstallDb,id,applicationContext);
    }
}
