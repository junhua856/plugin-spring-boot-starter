package cn.donting.spring.boot.plugin.core.servlet;

import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootWebManager;

/**
 * 插件的manager的 DispatcherServlet接口
 * @author donting
 * 2020-09-25 14:20
 */
public interface IPluginManagerDispatcherServlet extends IPluginDispatcherServlet {

}
