package cn.donting.spring.boot.plugin.core.servlet.de;

import cn.donting.spring.boot.plugin.core.util.FileUtil;
import cn.donting.spring.boot.plugin.core.util.Md5Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * @author donting
 * 2020-09-25 10:24
 */
public abstract class AbsServletApiSeparate implements ServletApiSeparate {
    static {
        File file = new File(ServletApiSeparate.servletApiPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    @Override
    public boolean isSeparate(URL[] libs) {
        try {
            InputStream inputStream = apiFileInputStream(libs);
            String fileMD5 = Md5Util.getInputStreamMD5(inputStream);
            String versionPath = ServletApiSeparate.servletApiPath + File.separator + "version";
            File v = new File(versionPath);
            if (!v.exists()) {
                v.createNewFile();
            }
            FileInputStream fileInputStream = new FileInputStream(v);
            byte[] bytes = new byte[fileInputStream.available()];
            fileInputStream.read(bytes);
            String s = new String(bytes);
            fileInputStream.close();
            if (!s.equals(fileMD5)) {
                File file = new File(ServletApiSeparate.servletApiPath);
                if (file.exists()) {
                    FileUtil.deleteSubFile(file);
                }
                v.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(v);
                fileOutputStream.write(fileMD5.getBytes());
                fileInputStream.close();

                return true;
            } else {
                return false;
            }

        } catch (Exception ex) {
            return false;
        }
    }
}
