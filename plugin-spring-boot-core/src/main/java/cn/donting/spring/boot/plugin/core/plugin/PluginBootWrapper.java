package cn.donting.spring.boot.plugin.core.plugin;

import cn.donting.spring.boot.plugin.core.PluginSpringApplicationRun;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import lombok.Getter;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author donting
 * 2020-09-25 11:33
 */
public class PluginBootWrapper implements PluginWrapper{
    private static final Log log = LogFactory.getLogger(PluginBootWrapper.class);

    private String springbootVersion;

    protected PluginSpringApplicationContext applicationContext;
    /**
     * Plugin 注解
     */
    protected final PluginInfo pluginInfo;
    @Getter
    protected final Class<?> pluginMainClass;

    @Getter
    protected final PluginClassLoader classLoader;

    @Getter
    private String[] args;

    protected final File file;

    private PluginStatus status;


    protected String temp;
    public PluginBootWrapper(File file, PluginInfo pluginInfo, Class<?> pluginMainClass, PluginClassLoader classLoader) {
        this.pluginInfo = pluginInfo;
        this.pluginMainClass = pluginMainClass;
        this.classLoader = classLoader;
        this.file = file;
        this.status=PluginStatus.STOP;
    }

    public boolean creatTemp() {
        String name = file.getName();
        if (name.endsWith(".jar")) {
            temp = PluginManager.PLUGIN_TEMP + File.separator + name.substring(0, name.length() - 4);
            File file = new File(temp);
            if (!file.exists()) {
                file.mkdirs();
                return true;
            }
            return false;
        }
//        if (name.equals("plugin-manager-dev.classpath")) {
//            temp = PluginManager.PLUGIN_TEMP + File.separator + "manager";
//            File file = new File(temp);
//            if (!file.exists()) {
//                file.mkdirs();
//            }
//            return;
//        }
        if (name.equals("plugin-dev.classpath")) {
            temp = PluginManager.PLUGIN_TEMP + File.separator + "plugin-dev";
            File file = new File(temp);
            if (!file.exists()) {
                file.mkdirs();
                return true;

            }
            return false;
        }
        log.warning(file.getPath() + "-->getTempPath ");
        return false;
    }

    @Override
    public PluginInfo getPluginInfo() {
        return pluginInfo;
    }

    @Override
    public String getId() {
        return pluginInfo.getId();
    }

    @Override
    public synchronized void start(String[] args) throws Exception {
        creatTemp();
        this.status=PluginStatus.STARTING;
        Class<?> aClass = classLoader.loadClass("cn.donting.spring.boot.starter.PluginRunSpringApplication");
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(classLoader);
        PluginSpringApplicationRun run = (PluginSpringApplicationRun)aClass.newInstance();
        run.init(pluginMainClass,args);
        PluginSpringApplicationContext notRefreshPluginSpringApplicationContext = run.getNotRefreshPluginSpringApplicationContext();
        notRefreshPluginSpringApplicationContext.setTempPath(getTempPath());
        applicationContext = run.run();
        this.springbootVersion=applicationContext.getSpringBootVersion();
        Thread.currentThread().setContextClassLoader(contextClassLoader);
        this.status=PluginStatus.RUNNING;
    }

    @Override
    public synchronized void stop() throws Exception {
        //TODO:stop
        applicationContext.exit();
        applicationContext=null;

    }

    @Override
    public synchronized void uninstall() throws Exception {
        //TODO:uninstall
    }

    @Override
    public String getTempPath() {
        return temp;
    }

    @Override
    public PluginStatus getStatus() {
        return status;
    }

    @Override
    public <T> T getBean(Class<T> type) throws Exception {
        return applicationContext.getBean(type);
    }

    @Override
    public <T> Map<String, T> getBeansOfType(Class<T> type) throws Exception {
        return applicationContext.getBeansOfType(type);
    }

    @Override
    public Object getBean(String name) throws Exception {
        return applicationContext.getBean(name);
    }

    @Override
    public String getSpringBootVersion() {
        return springbootVersion;
    }

    @Override
    @Deprecated
    public void setTempPath(String path) {

    }


}
