package cn.donting.spring.boot.plugin.core.plugin.info;

import cn.donting.spring.boot.plugin.core.plugin.PluginStatus;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import lombok.Getter;
import lombok.Setter;

/**
 * @author donting
 * 2020-08-14 08:58
 */
@Getter
@Setter
public class PluginInstallInfo {

    private PluginInfo pluginInfo;
    private PluginStatus status;
    private String installTime;
    private String statusChangeTime="";
    private String filePath;
    private String temp;
    private Boolean initialization;



    public PluginInstallInfo() {
    }

    public PluginInstallInfo(PluginInfo plugin) {
      this.pluginInfo=plugin;
    }
}
