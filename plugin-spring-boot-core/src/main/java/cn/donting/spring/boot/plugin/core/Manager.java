package cn.donting.spring.boot.plugin.core;

import java.lang.annotation.*;

/**
 * 标识 是插件管理者
 * @author donting
 * 2020-09-12 15:02
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Manager {
}
