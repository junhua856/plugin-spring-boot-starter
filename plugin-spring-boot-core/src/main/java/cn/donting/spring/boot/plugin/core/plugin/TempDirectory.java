package cn.donting.spring.boot.plugin.core.plugin;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;

/**
 * @author donting
 * 2020-09-29 10:32
 */
public interface TempDirectory {
    Log log= LogFactory.getLogger(TempDirectory.class);
    /**
     * @return temp TempDirectory Path
     */
    String getTempPath();
}
