package cn.donting.spring.boot.plugin.core.loader;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.util.Match;

import java.util.ArrayList;
import java.util.List;


/**
 * @author donting
 * 2020-09-12 18:03
 */
public class CommonLib {

   private static Log log= LogFactory.getLogger(CommonLib.class);

    private static final List<String> commonLibs = new ArrayList<String>(){{
        add("*plugin-spring-boot-core*");
        add("*fastjson*");
    }};


    public static boolean isCommonLib(String lib) {

        for (String commonLib : commonLibs) {
            if (Match.isMatch(lib, commonLib)) {
                return true;
            }
        }
        return false;
    }
}
