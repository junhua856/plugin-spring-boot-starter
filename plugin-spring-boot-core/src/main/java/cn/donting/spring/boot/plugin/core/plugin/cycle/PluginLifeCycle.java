package cn.donting.spring.boot.plugin.core.plugin.cycle;

import cn.donting.spring.boot.plugin.core.plugin.Order;

/**
 * plugin PluginLifeCycle
 * 生命周期
 *
 * @author donting
 * 2020-09-28 16:00
 */
public interface PluginLifeCycle  extends Order {
    void init();

    void update();

}
