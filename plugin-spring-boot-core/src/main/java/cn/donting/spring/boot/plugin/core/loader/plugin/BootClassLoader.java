package cn.donting.spring.boot.plugin.core.loader.plugin;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import org.springframework.boot.loader.ExecutableArchiveLauncher;
import org.springframework.boot.loader.LaunchedURLClassLoader;
import org.springframework.boot.loader.archive.Archive;
import org.springframework.boot.loader.archive.JarFileArchive;
import org.springframework.boot.loader.jar.JarFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.jar.Manifest;

/**
 * springboot loader 加载
 * @author donting
 * 2020-09-26 16:50
 */
public class BootClassLoader extends ExecutableArchiveLauncher {


    static final String BOOT_INF_CLASSES = "BOOT-INF/classes/";

    static final String BOOT_INF_LIB = "BOOT-INF/lib/";

    private final ClassLoader prent;
    static {
        JarFile.registerUrlProtocolHandler();
    }

    public BootClassLoader(File file, ClassLoader prent) throws IOException {
        super(jarFileToArchive(file));
        this.prent=prent;
    }


    public PluginClassLoader creatPluginClassLoader() throws Exception {
        ClassLoader classLoader = createClassLoader(getClassPathArchives());
        return (PluginClassLoader)classLoader;
    }

    @Override
    public String getMainClass() throws Exception {
        return super.getMainClass();
    }


    protected String getSpringBootVersion() throws Exception {
        Manifest manifest = getArchive().getManifest();
        String mainClass = null;
        if (manifest != null) {
            mainClass = manifest.getMainAttributes().getValue("Spring-Boot-Version");
        }
        return mainClass;
    }


    protected ClassLoader createClassLoader(URL[] urls) throws Exception {
        return new PluginClassLoader(urls, prent);
    }

    @Override
    protected boolean isNestedArchive(Archive.Entry entry) {
        if (entry.isDirectory()) {
            return entry.getName().equals(BOOT_INF_CLASSES);
        }
        return entry.getName().startsWith(BOOT_INF_LIB);
    }

    private static Archive jarFileToArchive(File file) throws IOException {
        return new JarFileArchive(file);
    }

}
