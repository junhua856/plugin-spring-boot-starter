package cn.donting.spring.boot.plugin.core.util;

import java.io.File;

/**
 * @author donting
 * 2020-09-22 11:12
 */
public class FileUtil {


    public static void delete(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File file1 : files) {
                FileUtil.delete(file1);
            }
            file.delete();
        } else {
            file.delete();
        }
    }
    public static void deleteSubFile(File file) {
        File[] files = file.listFiles();
        for (File file1 : files) {
            delete(file1);
        }
    }
    public static void delete(String path) {
        delete(new File(path));
    }

}
