package cn.donting.spring.boot.plugin.core.loader.plugin.dev;

import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginLoader;
import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.PluginBootWebWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginBootWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginInfoMake;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootManager;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * @author donting
 * 2020-09-25 14:46
 */
public class DevPluginLoader extends AbsDevLoader implements PluginLoader {
    private static final Log log= LogFactory.getLogger(DevPluginLoader.class);

    private final PluginInfoMake pluginMake;

    public DevPluginLoader(PluginInfoMake pluginMake) {
        this.pluginMake = pluginMake;
    }

    @Override
    public boolean isLoader(File file) {
        return file.getPath().equals(PluginClassLoader.DEV_PLUGIN_CLASSES);
    }

    @Override
    public PluginWrapper load(File file, ClassLoader prent) throws ReflectiveOperationException, IOException, PluginException {
        log.info("load dev:"+file.getPath());
        ClassPathEntity classPathEntity = creatURLForClassPath(file);
        PluginClassLoader pluginClassLoader=new PluginClassLoader(classPathEntity.getUrls(),prent);
        boolean isWeb = PluginBootManager.isWeb(pluginClassLoader);
        Class<?> aClass = pluginClassLoader.loadClass(classPathEntity.getMainClass());

        PluginInfo pluginInfo = null;
        try {
            pluginInfo = pluginMake.getPluginInfo(aClass);
        } catch (PluginException e) {
            throw  new PluginException("load PluginInfo fail",e);
        }

        PluginWrapper pluginWrapper = null;
        if (isWeb) {
            pluginWrapper = new PluginBootWebWrapper(file, pluginInfo, aClass, pluginClassLoader);
        } else {
            pluginWrapper = new PluginBootWrapper(file, pluginInfo, aClass, pluginClassLoader);

        }
        return pluginWrapper;
    }

    @Override
    public PluginInfoMake getPluginMake() {
        return pluginMake;
    }
}
