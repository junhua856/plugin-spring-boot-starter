package cn.donting.spring.boot.plugin.core.plugin.manger;

import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.exception.PluginMsgException;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.BootPluginLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.dev.DevPluginLoader;
import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;
import cn.donting.spring.boot.plugin.core.plugin.PluginBootWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.cycle.PluginLifeCycle;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInstallDb;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInstallInfo;
import cn.donting.spring.boot.plugin.core.util.OrderUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author donting
 * 2020-09-25 11:35
 */
public class PluginBootManager implements PluginManager {

    private static final Log log = LogFactory.getLogger(PluginBootManager.class);

    private final HashMap<String, PluginWrapper> plugs = new HashMap<>();


    private final PluginInstallDb pluginInstallDb;

    private final PluginSpringApplicationContext applicationContext;


    private final PluginClassLoader classLoader;

    private final String id;
    private String temp;

    /**
     *
     */
    static {
        File file = new File(PLUGIN_INSTALL_BASE);
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(PluginManager.PLUGIN_TEMP);
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(PluginManager.PLUGIN_BASE);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public PluginBootManager(PluginInstallDb pluginInstallDb, String id, PluginSpringApplicationContext applicationContext) {
        this.classLoader = (PluginClassLoader) Thread.currentThread().getContextClassLoader();
        this.pluginInstallDb = pluginInstallDb;
        this.id = id;
        this.applicationContext = applicationContext;
        applicationContext.setTempPath(getTempPath());
    }

    public static boolean isWeb(ClassLoader classLoader) {
        boolean isWeb = false;
        try {
            Class<?> aClass = classLoader.loadClass("org.springframework.web.WebApplicationInitializer");
            if (aClass != null) {
                //web
                isWeb = true;
            }
        } catch (Exception ex) {
            //not web
            ex.printStackTrace();
        }
        return isWeb;
    }

    @Override
    public PluginWrapper getRunning(String pluginId) {
        return plugs.get(pluginId);
    }

    @Override
    public void loadDevPlugin() throws Exception {
        File file = new File(PluginClassLoader.DEV_PLUGIN_CLASSES);
        if (!file.exists()) {
            return;
        }
        PluginLoader pluginLoader = getPluginLoader(file);
        PluginWrapper load = pluginLoader.load(file, classLoader.getParent());
        plugs.put(load.getId(), load);
        load.start(new String[]{});
    }

    @Override
    public synchronized void stop(String pluginId) throws Exception {
        PluginWrapper remove = plugs.remove(pluginId);
        if (remove == null) {
            throw new PluginMsgException(pluginId + " is not running");
        }
        log.info("stopping plugin :" + pluginId);
        remove.stop();
        pluginInstallDb.stop(pluginId);
    }


    @Override
    public synchronized void start(String pluginId) throws Exception {
        if (plugs.containsKey(pluginId)) {
            throw new PluginMsgException(pluginId + " is Started");
        }
        log.info("Starting plugin......" + pluginId);
        PluginInstallInfo pluginInstallInfo = pluginInstallDb.get(pluginId);
        File file = new File(pluginInstallInfo.getFilePath());
        PluginLoader pluginLoader = getPluginLoader(file);
        PluginWrapper load = pluginLoader.load(file, classLoader.getParent());
        load.start(new String[]{});
        PluginInstallInfo start = pluginInstallDb.start(pluginId);
        log.info("Started plugin");
        if (!pluginInstallInfo.getInitialization()) {
            Map<String, PluginLifeCycle> beansOfType = load.getBeansOfType(PluginLifeCycle.class);
            List<PluginLifeCycle> pluginLifeCycles = OrderUtil.orderByOrder(beansOfType, true);
            for (PluginLifeCycle pluginLifeCycle : pluginLifeCycles) {
                pluginLifeCycle.init();
            }
            start.setInitialization(true);
            pluginInstallDb.update(pluginInstallInfo);
        }
        plugs.put(load.getId(), load);
    }

    @Override
    public synchronized String install(byte[] bytes) throws Exception {
        log.info("install plugin......");
        File file = new File(PLUGIN_INSTALL_BASE + File.separator + System.currentTimeMillis() + ".jar");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bytes);
        fileOutputStream.close();
        PluginLoader pluginLoader = getPluginLoader(file);
        PluginWrapper load = pluginLoader.load(file, classLoader.getParent());
        PluginInstallInfo pluginInstallInfo = pluginInstallDb.get(load.getId());
        if (pluginInstallInfo != null) {
            file.delete();
            throw new PluginMsgException("该插件已安装");
        }
        pluginInstallDb.install(load.getPluginInfo(), file.getPath(), load.getTempPath());
        load.getSpringBootVersion();
        log.info("installed pluginId:" + load.getId());
        return load.getId();
    }

    @Override
    public PluginLoader getPluginLoader(File file) {
        try {
            Map<String, PluginLoader> beansOfType = applicationContext.getBeansOfType(PluginLoader.class);
            for (Map.Entry<String, PluginLoader> stringPluginLoaderEntry : beansOfType.entrySet()) {
                if (stringPluginLoaderEntry.getValue().isLoader(file)) {
                    return stringPluginLoaderEntry.getValue();
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public synchronized void update(byte[] bytes, String pluginId) throws Exception {
        PluginWrapper remove = plugs.remove(pluginId);
        if (remove != null) {
            remove.stop();
            pluginInstallDb.start(pluginId);
        }
        PluginInstallInfo pluginInstallInfo = pluginInstallDb.get(pluginId);
        if (pluginInstallInfo == null) {
            throw new PluginException(pluginId + " not install");
        }
        String filePath = pluginInstallInfo.getFilePath();
        File file = new File(PluginManager.PLUGIN_TEMP + System.currentTimeMillis() + "-temp.jar");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bytes);
        fileOutputStream.close();
        PluginLoader pluginLoader = getPluginLoader(file);
        PluginWrapper load = pluginLoader.load(file, classLoader.getParent());
        if (!load.getPluginInfo().getId().equals(pluginId)) {
            file.delete();
            throw new PluginException(pluginId + " != " + load.getPluginInfo().getId());
        }
        if (pluginInstallInfo.getPluginInfo().getVersionCode() <= load.getPluginInfo().getVersionCode()) {
            throw new PluginException("版本 <= 以安装的版本：" + pluginInstallInfo.getPluginInfo().getVersionCode() + "<=" + load.getPluginInfo().getVersionCode());
        }
        file.delete();
        file = new File(pluginInstallInfo.getFilePath());
        fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bytes);
        fileOutputStream.close();
        pluginInstallDb.update(load.getPluginInfo());
    }


    @Override
    public String getTempPath() {
        if(temp==null){
            creatTemp();
        }
        return temp;
    }
    public boolean creatTemp() {
        File file = new File(PLUGIN_TEMP + File.separator + "manager");
        this.temp=file.getPath();
        if (!file.exists()) {
            file.mkdirs();
            return true;
        } else {
            return false;
        }
    }
}
