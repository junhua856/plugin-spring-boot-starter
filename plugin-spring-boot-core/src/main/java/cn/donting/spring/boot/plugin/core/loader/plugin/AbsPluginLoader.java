package cn.donting.spring.boot.plugin.core.loader.plugin;

import cn.donting.spring.boot.plugin.core.Plugin;
import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.exception.PluginRuntimeException;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginBootWebWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginBootWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.util.Match;
import org.springframework.boot.loader.ExecutableArchiveLauncher;
import org.springframework.boot.loader.LaunchedURLClassLoader;
import org.springframework.boot.loader.archive.JarFileArchive;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @author donting
 * 2020-09-26 15:07
 */
public abstract class AbsPluginLoader  implements PluginLoader {


    protected PluginWrapper load(File file, String mainClass, PluginClassLoader classLoader) throws PluginException, ClassNotFoundException, IOException {
        boolean isWeb = false;
        for (URL url : classLoader.getURLs()) {
            if (Match.isMatch(url.getPath(), "*spring-boot-starter-web*")) {
                isWeb = true;
                break;
            }
        }
        Class<?> aClass = classLoader.loadClass(mainClass);
        Plugin annotation = aClass.getAnnotation(Plugin.class);
        if (annotation == null) {
            throw new PluginException("不是一个插件");
        }
        PluginInfo pluginInfo = new PluginInfo(annotation);
        PluginWrapper pluginWrapper = null;
        if (isWeb) {
            try {
                classLoader.loadClass("javax.servlet.http.HttpServlet");
                pluginWrapper = new PluginBootWebWrapper(file, pluginInfo, aClass, classLoader);
            } catch (Exception ex) {
                throw new PluginRuntimeException("manager 不是一个web插件，因此不能加载web插件");
            }
        } else {
            pluginWrapper = new PluginBootWrapper(file, pluginInfo, aClass, classLoader);
        }
        classLoader.setName(pluginWrapper.getId());
        return pluginWrapper;
    }

}
