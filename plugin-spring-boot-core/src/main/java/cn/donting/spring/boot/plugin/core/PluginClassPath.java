package cn.donting.spring.boot.plugin.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author donting
 * 2020-09-24 14:40
 */
public class PluginClassPath {

    public static void generateClassPathFile(Class mainClass,String filePath) throws IOException {
        generateClassPathFile(mainClass,filePath,(URLClassLoader) Thread.currentThread().getContextClassLoader());
    }
    public static void generateClassPathFile(Class mainClass,String filePath, URLClassLoader classLoader) throws IOException {
        File file=new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        URL[] urLs = classLoader.getURLs();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(mainClass.getCanonicalName());
        URL location = mainClass.getProtectionDomain().getCodeSource().getLocation();
        stringBuffer.append("\n" + location.toString());
        boolean dev = true;
        for (URL urL : urLs) {
            if (urL.equals(location)) {
                dev = false;
                continue;
            }
            if (dev) {
                continue;
            }
            stringBuffer.append("\n" + urL.toString());
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(stringBuffer.toString().getBytes("utf-8"));
        fileOutputStream.close();
    }
}
