package cn.donting.spring.boot.plugin.core.plugin;

/**
 * @author donting
 * 2020-09-26 22:29
 */
public interface PluginMainClass {
    Class<?> getMainClass();
    void setMainClass(Class<?> mainClass);
    void setTempPath(String path);
}
