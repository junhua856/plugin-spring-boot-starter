package cn.donting.spring.boot.plugin.core.loader.plugin.dev;

import cn.donting.spring.boot.plugin.core.Plugin;
import cn.donting.spring.boot.plugin.core.PluginSpringApplicationRun;
import cn.donting.spring.boot.plugin.core.loader.CommonLib;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginManagerClassLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.PluginManagerLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootManager;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootWebManager;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

/**
 * @author donting
 * 2020-09-25 11:16
 */
public class DevManagerLoader extends AbsDevLoader implements PluginManagerLoader {


    @Override
    public void run(File file) throws IOException, ReflectiveOperationException {
        ClassPathEntity classPathEntity = creatURLForClassPath(file);
        URLClassLoader contextClassLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
        Method addURL = contextClassLoader.getClass().getSuperclass().getDeclaredMethod("addURL", URL.class);
        addURL.setAccessible(true);
        for (URL url : classPathEntity.getCommonLib()) {
            addURL.invoke(contextClassLoader, url);
        }
        PluginManagerClassLoader managerClassLoader = new PluginManagerClassLoader(classPathEntity.getUrls(), contextClassLoader);
        Thread.currentThread().setContextClassLoader(managerClassLoader);
        Class<?> aClass = managerClassLoader.loadClass("cn.donting.spring.boot.starter.PluginRunSpringApplication");
        PluginSpringApplicationRun run = (PluginSpringApplicationRun)aClass.newInstance();
        Class<?> mainClass = managerClassLoader.loadClass(classPathEntity.getMainClass());
        run.init(mainClass, new String[0]);
        PluginSpringApplicationContext run1 = run.run();
    }
}
