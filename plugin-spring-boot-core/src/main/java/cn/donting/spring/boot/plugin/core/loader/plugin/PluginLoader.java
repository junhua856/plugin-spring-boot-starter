package cn.donting.spring.boot.plugin.core.loader.plugin;

import cn.donting.spring.boot.plugin.core.plugin.PluginInfoMake;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;

import java.io.File;

/**
 * 插件加载器
 * @author donting
 * 2020-09-25 11:13
 */
public interface PluginLoader {

    /**
     * 是否能加载
     * @param file
     * @return
     */
    boolean isLoader(File file);

    /**
     * 加载
     * @param file 文件
     * @param prent 父加载器
     * @return PluginWrapper
     * @throws Exception Exception
     */
    PluginWrapper load(File file,ClassLoader prent) throws Exception;

    PluginInfoMake getPluginMake();
}
