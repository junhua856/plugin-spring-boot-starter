package cn.donting.spring.boot.plugin.core.loader.classes;

import cn.donting.spring.boot.plugin.core.logging.Log;
import cn.donting.spring.boot.plugin.core.logging.LogFactory;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author donting
 * 2020-09-24 21:21
 */
public class PluginClassLoader extends URLClassLoader {

    public static final String DEV_PLUGIN_MANAGER_CLASSES = System.getProperty("user.dir") + File.separator + "plugin-manager-dev.classpath";
    public static final String DEV_PLUGIN_CLASSES = System.getProperty("user.dir") + File.separator + "plugin-dev.classpath";

    private static final Log log = LogFactory.getLogger(PluginClassLoader.class);
    private String name;

    public PluginClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    public PluginClassLoader(URL[] urls, ClassLoader parent, String name) {
        super(urls, parent);
        this.name = name;
    }

    public void setName(String name) {
        if (this.name == null) {
            this.name = name;
        }
    }

    @Override
    public void addURL(URL url) {
        super.addURL(url);
    }

    @Override
    protected void finalize() throws Throwable {
        log.info("PluginClassLoader "+name +" is finalize");
    }
}
