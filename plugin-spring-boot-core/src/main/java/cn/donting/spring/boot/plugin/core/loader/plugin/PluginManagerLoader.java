package cn.donting.spring.boot.plugin.core.loader.plugin;

import java.io.File;
import java.io.IOException;

/**
 * @author donting
 * 2020-09-26 20:50
 */
public interface PluginManagerLoader {

    void run(File file) throws IOException, ReflectiveOperationException;
}
