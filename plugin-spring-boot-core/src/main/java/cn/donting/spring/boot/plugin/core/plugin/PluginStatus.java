package cn.donting.spring.boot.plugin.core.plugin;

/**
 * @author donting
 * 2020-09-25 11:48
 */
public enum PluginStatus {
    RUNNING,STOP,STOPPING,STARTING
}
