package cn.donting.spring.boot.plugin.core.servlet.de;

import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author donting
 * 2020-09-24 21:14
 */
public interface ServletApiSeparate {

    String servletApiPath= PluginManager.PLUGIN_BASE+File.separator +"servletApi";
    /**
     * 分离Servlet Api
     * @param savePath 分离后保存路径
     * @param libs lib的路径
     */
    void separate(String savePath, URL[] libs);

    boolean isSeparate(URL[] libs);

    InputStream apiFileInputStream(URL[] libs) throws IOException;
}
