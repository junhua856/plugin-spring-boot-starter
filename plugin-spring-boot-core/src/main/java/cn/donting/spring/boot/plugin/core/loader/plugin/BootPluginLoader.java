package cn.donting.spring.boot.plugin.core.loader.plugin;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginInfoMake;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;

import java.io.File;

/**
 * springboot loader
 *
 * @author donting
 * 2020-09-26 14:51
 */
public class BootPluginLoader extends AbsPluginLoader {


    private final PluginInfoMake pluginMake;

    public BootPluginLoader(PluginInfoMake pluginMake) {
        this.pluginMake = pluginMake;
    }

    @Override
    public boolean isLoader(File file) {
        return file.getName().endsWith(".jar");
    }

    @Override
    public PluginWrapper load(File file, ClassLoader prent) throws Exception {
        BootClassLoader bootClassLoader = new BootClassLoader(file, prent);
        PluginClassLoader pluginClassLoader = bootClassLoader.creatPluginClassLoader();
        String mainClass = bootClassLoader.getMainClass();
        PluginWrapper load = super.load(file, mainClass, pluginClassLoader);
        load.getPluginInfo().setSpringbootVersion(bootClassLoader.getSpringBootVersion());
        return load;
    }

    @Override
    public PluginInfoMake getPluginMake() {
        return pluginMake;
    }
}
