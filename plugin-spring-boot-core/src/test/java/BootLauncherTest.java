import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.BootClassLoader;

import java.io.File;

/**
 * @author donting
 * 2020-09-26 16:56
 */
public class BootLauncherTest {
    public static void main(String[] args) throws Exception {
        BootClassLoader bootLauncher=new BootClassLoader(new File("/Users/donting/工作空间/d" +
                "ev/workspace/plugin-spring-boot-starter-prent/test-p" +
                "lugin/target/test-plugin-1.1-SNAPSHOT.jar"),ClassLoader.getSystemClassLoader());
        PluginClassLoader pluginClassLoader = bootLauncher.creatPluginClassLoader();
        Class<?> aClass = pluginClassLoader.loadClass("org.springframework.boot.autoconfigure.SpringBootApplication");
        System.out.println(pluginClassLoader);
    }
}
