# plugin-spring-boot-starter

#### 介绍
springboot 插件化开发框架，动态加载基于Spring boot开发的插件。

1. 插件可动态加载与卸载。
2. springBoot的插件，不仅可以作为插件加载，还能单独以springboot的方式运行。(插件，单独运行不冲突)。
3. springboot改造简单，添加依赖，更改启动方式，添加注解，三步即可将Springboot 改造为可加载插件(保留原始运行方式)。
4. springboot打包方式不变，按照springboot 打包即可。
5. 插件隔离，不同插件可使用不同版本的依赖包。
6. 插件理论支持不同的springboot版本。
7. 理论支持所有的SpringBoot starter(官方与非官方)(aop，mybatis....)。

##### 与springboot差异
1. spring-boot-starter-web：
    1. 暂不支持 @Autowired 注入Http相关类：HttpServletRequest,HttpServletResponse....,支持方法注入方式。
2. 暂不支持 JMX



