package test.plugin;

import cn.donting.spring.boot.plugin.core.Plugin;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.starter.PluginSpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author donting
 * 2020-09-25 09:03
 */
@SpringBootApplication
@Plugin(id = "plugin1",name = "插件1",version ="1",versionCode = 1)
public class TestPlugin {
    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException, InterruptedException {
        PluginSpringApplication.runApplication(TestPlugin.class,args);
    }
}
