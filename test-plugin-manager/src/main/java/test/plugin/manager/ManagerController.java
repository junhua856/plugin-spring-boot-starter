package test.plugin.manager;

import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

/**
 * @author donting
 * 2020-09-26 14:31
 */
@RestController
public class ManagerController {
    @Autowired
    PluginManager pluginManager;


    @GetMapping("stop")
    public String stop() throws Exception {
        pluginManager.stop("plugin1");
        return "ok";
    }

    @GetMapping("stare")
    public String stare() throws Exception {
        pluginManager.start("plugin1");
        return "ok----";
    }

    @GetMapping("install")
    public String install() throws Exception {
        pluginManager.install(new File("/Users/donting/工作空间/dev/workspace/plugin-spring-boot-starter-prent/test-plugin/target/test-plugin-1.1-SNAPSHOT.jar"));

        return "ok----";
    }


}
