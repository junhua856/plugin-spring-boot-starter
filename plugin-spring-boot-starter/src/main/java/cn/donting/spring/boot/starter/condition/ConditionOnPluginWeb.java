package cn.donting.spring.boot.starter.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * 是否是 web
 * @author donting
 * 2020-09-25 16:44
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Conditional(PluginWebOnCondition.class)
public @interface ConditionOnPluginWeb {
    boolean value() default true;
}
