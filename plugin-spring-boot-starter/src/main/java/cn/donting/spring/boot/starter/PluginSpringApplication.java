package cn.donting.spring.boot.starter;

import cn.donting.spring.boot.plugin.core.Manager;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginClassLoader;
import cn.donting.spring.boot.plugin.core.loader.classes.PluginManagerClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.PluginMainClass;
import cn.donting.spring.boot.plugin.core.plugin.PluginSpringApplicationContext;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootManager;
import cn.donting.spring.boot.starter.context.PluginAnnotationConfigApplicationContext;
import cn.donting.spring.boot.starter.context.PluginManagerAnnotationConfigServletWebServerApplicationContext;
import cn.donting.spring.boot.starter.context.PluginWebAnnotationConfigApplicationContext;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;

/**
 * 插件的启动类
 * manager
 * plugin
 *
 * @author donting
 * 2020-09-24 21:07
 */
public class PluginSpringApplication extends SpringApplication {

    private static final String PLUGIN_DEFAULT_SERVLET_WEB_CONTEXT_CLASS = "cn.donting.spring.boot.starter.context." +
            "PluginAnnotationConfigServletWebServerApplicationContext";
    private static final String PLUGIN_DEFAULT_CONTEXT_CLASS = "cn.donting.spring.boot.starter.context." +
            "PluginAnnotationConfigApplicationContext";

    private PluginSpringApplicationContext applicationContext;
    private ConfigurableApplicationContext configurableApplicationContext;
    private final Type type;
    private final Class<?> mainApplicationClass;

    public PluginSpringApplication(Type type, Class<?> mainApplicationClass) {
        super(mainApplicationClass);
        if (type == Type.NONE) {
            setWebApplicationType(WebApplicationType.NONE);
        }
        this.type = type;
        setMainApplicationClass(mainApplicationClass);
        this.mainApplicationClass = mainApplicationClass;
        creatConfigurableApplicationContext();
    }

    private void creatConfigurableApplicationContext() {
        boolean isManager = false;
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if (contextClassLoader instanceof PluginManagerClassLoader) {
            isManager = true;
        }
        Class<?> contextClass;
        try {
            switch (getWebApplicationType()) {
                case SERVLET:
                    if (isManager) {
                        contextClass = Class.forName(PluginManagerAnnotationConfigServletWebServerApplicationContext
                                .class.getCanonicalName());
                    } else {
                        contextClass = Class.forName(PluginWebAnnotationConfigApplicationContext.class.getCanonicalName());
                    }
                    break;
                case REACTIVE:
                    throw new RuntimeException("REACTIVE is TODO");
                default:
                    if (isManager) {
                        contextClass = Class.forName(PluginAnnotationConfigApplicationContext
                                .class.getCanonicalName());
                    } else {
                        if (PluginBootManager.isWeb(contextClassLoader)) {
                            contextClass = Class.forName(PluginWebAnnotationConfigApplicationContext.class.getCanonicalName());
                        } else {
                            contextClass = Class.forName(PluginAnnotationConfigApplicationContext
                                    .class.getCanonicalName());
                        }
                    }


            }
        } catch (ClassNotFoundException ex) {
            throw new IllegalStateException(
                    "Unable create a default ApplicationContext, please specify an ApplicationContextClass", ex);
        }
        ConfigurableApplicationContext o = (ConfigurableApplicationContext) BeanUtils.instantiateClass(contextClass);
        PluginMainClass pluginMainClass = (PluginMainClass) o;
        pluginMainClass.setMainClass(mainApplicationClass);
        applicationContext = (PluginSpringApplicationContext) o;
        configurableApplicationContext = o;
    }

    @Override
    protected ConfigurableApplicationContext createApplicationContext() {
        return configurableApplicationContext;
    }

    public PluginSpringApplicationContext getApplicationContext() {
        return applicationContext;
    }


    private static PluginSpringApplicationContext runManager(Class<?> mainClass) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        PluginSpringApplication pluginSpringApplication;
        if (PluginBootManager.isWeb(contextClassLoader)) {
            pluginSpringApplication = new PluginSpringApplication(Type.WEB, mainClass);
        } else {
            pluginSpringApplication = new PluginSpringApplication(Type.NONE, mainClass);
        }
        PluginSpringApplicationContext run = (PluginSpringApplicationContext) pluginSpringApplication.run();
        return run;
    }


    private static PluginSpringApplicationContext runPlugin(Class<?> mainClass) {
        PluginSpringApplication pluginSpringApplication = new PluginSpringApplication(Type.NONE, mainClass);
        PluginSpringApplicationContext run = (PluginSpringApplicationContext) pluginSpringApplication.run();
        return run;
    }


    public static PluginSpringApplicationContext run(Class<?> mainClass) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if (contextClassLoader instanceof PluginManagerClassLoader) {
            return runManager(mainClass);
        } else {
            return runPlugin(mainClass);
        }

    }


    /**
     * 开发是的启动运行方法
     *
     * @param mainClass
     * @param args
     */
    public static void runApplication(Class<?> mainClass, String[] args) {
        //TODO: jar 独立运行
        Manager annotation = mainClass.getAnnotation(Manager.class);
        String classPath = PluginClassLoader.DEV_PLUGIN_CLASSES;
        if (annotation != null) {
            //manager
//            //拆servlet api---
//            String savePath = ServletApiSeparate.servletApiPath;
//            ServletApiSeparate servletApiSeparate = ServletApiSeparateFactory.getServletApiSeparate();
//            URLClassLoader contextClassLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
//            URL[] urLs = contextClassLoader.getURLs();
//            if (servletApiSeparate.isSeparate(urLs)) {
//                servletApiSeparate.separate(savePath, urLs);
//            }
            //写classesPath
            classPath = PluginClassLoader.DEV_PLUGIN_MANAGER_CLASSES;
        }
        try {
            File file = new File(classPath);
            if (!file.exists()) {

                file.createNewFile();

            }
            URLClassLoader urlClassLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
            URL[] urLs = urlClassLoader.getURLs();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(mainClass.getCanonicalName());
            URL location = mainClass.getProtectionDomain().getCodeSource().getLocation();
            boolean isLib = false;
            for (URL urL : urLs) {
                if (isLib) {
                    stringBuffer.append("\n" + urL.toString());
                    continue;
                }
                if (location.equals(urL)) {
                    stringBuffer.append("\n" + urL.toString());
                    isLib = true;
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(stringBuffer.toString().getBytes());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    static enum Type {
        WEB, NONE
    }
}
