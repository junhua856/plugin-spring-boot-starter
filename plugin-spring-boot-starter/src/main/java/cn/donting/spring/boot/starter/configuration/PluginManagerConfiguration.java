package cn.donting.spring.boot.starter.configuration;

import cn.donting.spring.boot.plugin.core.exception.PluginException;
import cn.donting.spring.boot.plugin.core.loader.plugin.BootPluginLoader;
import cn.donting.spring.boot.plugin.core.loader.plugin.dev.DevPluginLoader;
import cn.donting.spring.boot.plugin.core.plugin.*;
import cn.donting.spring.boot.plugin.core.plugin.cycle.PluginLifeCycle;
import cn.donting.spring.boot.plugin.core.plugin.info.DefaultPluginInstallDb;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInfo;
import cn.donting.spring.boot.plugin.core.plugin.info.PluginInstallDb;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootManager;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootWebManager;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import cn.donting.spring.boot.plugin.core.util.OrderUtil;
import cn.donting.spring.boot.starter.condition.ConditionOnPluginManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * manager 配置类
 *
 * @author donting
 * 2020-09-25 18:17
 */
@Configurable
@ConditionOnPluginManager
public class PluginManagerConfiguration implements ApplicationRunner {

    private static final Log logger = LogFactory.getLog(PluginManagerConfiguration.class);

    public PluginManagerConfiguration() {
        logger.info("PluginManagerConfiguration .......");
    }

    @Autowired
    ApplicationContext applicationContext;

    @Bean
    @ConditionalOnMissingBean(PluginManager.class)
    public PluginManager pluginManager(PluginInstallDb pluginInstallDb, PluginInfoMake pluginInfoMake, ApplicationContext applicationContext) throws PluginException {
        PluginMainClass pluginMainClass = (PluginMainClass) applicationContext;
        Class<?> mainClass = pluginMainClass.getMainClass();
        PluginInfo pluginInfo = pluginInfoMake.getPluginInfo(mainClass);
        PluginManager pluginManager;
        if (PluginBootManager.isWeb(Thread.currentThread().getContextClassLoader())) {
            pluginManager = new PluginBootWebManager(pluginInstallDb, pluginInfo.getId(), (PluginSpringApplicationContext) applicationContext);
        } else {
            pluginManager = new PluginBootManager(pluginInstallDb, pluginInfo.getId(), (PluginSpringApplicationContext) applicationContext);
        }
        PluginSpringApplicationContext pluginSpringApplicationContext= (PluginSpringApplicationContext)applicationContext;
        pluginSpringApplicationContext.setTempPath(pluginManager.getTempPath());
        return pluginManager;
    }

    @Bean
    @ConditionalOnMissingBean(PluginInstallDb.class)
    public PluginInstallDb pluginInstallDb() {
        return new DefaultPluginInstallDb();
    }

    @Bean
    @ConditionalOnMissingBean(PluginInfoMake.class)
    public PluginInfoMake pluginMake() {
        return new DefaultPluginMake();
    }

    @Bean
    public BootPluginLoader bootPluginLoader(PluginInfoMake pluginMake) {
        return new BootPluginLoader(pluginMake);
    }

    @Bean
    public DevPluginLoader devPluginLoader(PluginInfoMake pluginMake) {
        return new DevPluginLoader(pluginMake);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        PluginManager pluginManager = applicationContext.getBean(PluginManager.class);

        //是否进行初始化
        if (pluginManager.initializing()) {
            logger.info("Initializing.....");
            Map<String, PluginLifeCycle> beansOfType = applicationContext.getBeansOfType(PluginLifeCycle.class);
            List<PluginLifeCycle> pluginLifeCycles = OrderUtil.orderByOrder(beansOfType, true);
            for (PluginLifeCycle pluginLifeCycle : pluginLifeCycles) {
                pluginLifeCycle.init();
            }
        }
         pluginManager.loadDevPlugin();
    }
}
