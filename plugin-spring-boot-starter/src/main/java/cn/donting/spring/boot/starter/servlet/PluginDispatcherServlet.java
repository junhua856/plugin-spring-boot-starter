package cn.donting.spring.boot.starter.servlet;

import cn.donting.spring.boot.plugin.core.plugin.PluginBootWebWrapper;
import cn.donting.spring.boot.plugin.core.plugin.PluginWrapper;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootWebManager;
import cn.donting.spring.boot.plugin.core.servlet.IPluginDispatcherServlet;
import cn.donting.spring.boot.plugin.core.servlet.IPluginManagerDispatcherServlet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author donting
 * 2020-09-25 09:29
 */
public class PluginDispatcherServlet extends DispatcherServlet implements IPluginDispatcherServlet {
    protected static final Log log = LogFactory.getLog(PluginDispatcherServlet.class);


    public PluginDispatcherServlet() {
    }

    public PluginDispatcherServlet(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        super.setApplicationContext(applicationContext);
        onRefresh(applicationContext);
    }

    @Override
    public void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super.doService(request,response);
    }
}
