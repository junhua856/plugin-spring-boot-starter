package cn.donting.spring.boot.starter.condition;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginManagerClassLoader;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Map;

/**
 * @author donting
 * 2020-09-25 16:46
 */
public class PluginManagerOnCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();

        Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes(ConditionOnPluginManager.class.getName());
        boolean value = (boolean) annotationAttributes.get("value");

        boolean isManager;
        if (contextClassLoader instanceof PluginManagerClassLoader) {
            isManager = true;
        } else {
            isManager = false;
        }
        return isManager == value;

    }
}
