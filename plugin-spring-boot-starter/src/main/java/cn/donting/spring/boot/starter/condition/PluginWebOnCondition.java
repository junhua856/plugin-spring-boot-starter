package cn.donting.spring.boot.starter.condition;

import cn.donting.spring.boot.plugin.core.loader.classes.PluginManagerClassLoader;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginBootManager;
import com.sun.webkit.plugin.PluginManager;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Map;

/**
 * @author donting
 * 2020-09-25 16:46
 */
public class PluginWebOnCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if (PluginBootManager.isWeb(contextClassLoader)) {
            return true;
        }
        return false;

    }
}
