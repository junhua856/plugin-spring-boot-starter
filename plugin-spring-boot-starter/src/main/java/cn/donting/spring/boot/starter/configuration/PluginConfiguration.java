package cn.donting.spring.boot.starter.configuration;

import cn.donting.spring.boot.plugin.core.plugin.TempDirectory;
import cn.donting.spring.boot.plugin.core.plugin.TempPath;
import cn.donting.spring.boot.plugin.core.plugin.manger.PluginManager;
import cn.donting.spring.boot.starter.condition.ConditionOnPluginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.File;

/**
 * 插件,mannager 都加载
 *
 * @author donting
 * 2020-09-29 10:42
 */
@Configurable
public class PluginConfiguration {

    @Bean(name = "tempDirectory")
    public TempDirectory tempDirectory(ApplicationContext applicationContext) {
        TempPath tempPath;
        tempPath = new TempPath(((TempDirectory) applicationContext).getTempPath());
        return tempPath;
    }


}
